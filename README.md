SER502 Project1 (Spring 2015)

R Programming Language

Team12:


Richa Mittal | 
Aditya Narasimhamurthy | 
Neeraj Bahl

Arizona State University

-----------------------------------------------------------------------
Youtube Video on R Programming Language:
https://www.youtube.com/watch?v=sx3lRsCoTDU
-----------------------------------------------------------------------
R Programming Language and R Studio IDE Installation Instructions:

Install R
Windows: http://cran.r-project.org/bin/windows/base/
Mac: http://cran.r-project.org/bin/macosx/
Version: 3.1.2

RStudio Download:
http://www.rstudio.com/products/rstudio/download/
Version: 0.98.1102
-----------------------------------------------------------------------
R Programming Language Running Instructions:
Run the commands as provided in the sample code as a set of commands in the R Console or as a script in R Script Editor using default R Console or RStudio IDE